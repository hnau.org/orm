package org.hnau.orm


data class ExtractInfo<out K, out T, P>(
        val keys: Iterable<K>,
        val extract: P.() -> T
)