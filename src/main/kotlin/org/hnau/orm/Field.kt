package org.hnau.orm

import org.hnau.base.data.mapper.Mapper


data class Field<K, T, R, P>(
        val key: K,
        val typeAdapter: TypeAdapter<K, T, R, P>
) {

    val read: P.() -> T =
            { typeAdapter.read(this, key) }

    val write: R.(T) -> Unit =
            { value -> typeAdapter.write(this, key, value) }

    companion object

}

fun <K, T, R, P> Field<K, T, R, P>.writer(
        value: T
) = typeAdapter.writer(
        key = key,
        value = value
)

fun <K, T, R, P> Field<K, T, R, P>.keyWithWriter(value: T) =
        key to writer(value)