package org.hnau.orm

import org.hnau.base.data.mapper.Mapper


data class TypeAdapter<K, T, R, P>(
        val read: P.(K) -> T,
        val write: R.(K, T) -> Unit
) {

    companion object

}

fun <K, I, O, R, P> TypeAdapter<K, I, R, P>.map(
        mapper: Mapper<I, O>
) = TypeAdapter<K, O, R, P>(
        read = { key -> read(key).let(mapper.direct) },
        write = { key, value -> write(key, mapper.reverse(value)) }
)

fun <K, T, R, P> TypeAdapter<K, T, R, P>.writer(
        key: K,
        value: T
): R.() -> Unit = {
    write(key, value)
}