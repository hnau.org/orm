package org.hnau.orm.iterator



interface ClosableIterable<T>: Iterable<T>, AutoCloseable