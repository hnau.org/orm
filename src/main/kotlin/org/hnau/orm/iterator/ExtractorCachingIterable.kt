package org.hnau.orm.iterator

import org.hnau.base.extensions.boolean.checkBoolean
import org.hnau.base.extensions.boolean.ifFalse


class ExtractorCachingIterable<T>(
        private val iterateInfo: IterateInfo<T>
) : ClosableRelay(), ClosableIterable<T> {

    private val cachedValues = ArrayList<T>()

    private inline fun <R> synchronized(block: () -> R) =
            synchronized(lock = this, block = block)

    override fun executeClose() = iterateInfo.close()

    override fun iterator() = object : Iterator<T> {

        private var current = -1

        private fun cacheAfterCurrentIfExistsUnsafe() =
                (current < cachedValues.size - 1).checkBoolean(
                        ifTrue = { true },
                        ifFalse = {
                            checkIsClosed(
                                    ifClosed = { false },
                                    ifNotClosed = {
                                        iterateInfo.switchLine().apply {
                                            checkBoolean<Unit>(
                                                    ifTrue = {
                                                        iterateInfo.extract().also { value ->
                                                            cachedValues.add(value)
                                                        }
                                                    },
                                                    ifFalse = ::forceClose
                                            )
                                        }
                                    }
                            )
                        }
                )

        override fun hasNext() =
                synchronized { cacheAfterCurrentIfExistsUnsafe() }

        override fun next() = synchronized {
            cacheAfterCurrentIfExistsUnsafe().ifFalse {
                throw IndexOutOfBoundsException("Unable to extract ${current + 1} row")
            }
            current++
            cachedValues[current]
        }
    }

}