package org.hnau.orm.iterator



data class IterateInfo<T>(
        val switchLine: () -> Boolean,
        val extract: () -> T,
        val close: () -> Unit
)