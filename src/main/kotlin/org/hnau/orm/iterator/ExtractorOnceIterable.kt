package org.hnau.orm.iterator

import org.hnau.base.extensions.boolean.ifFalse
import org.hnau.base.extensions.boolean.ifTrue


class ExtractorOnceIterable<T>(
        private val iterateInfo: IterateInfo<T>
) : ClosableRelay(), ClosableIterable<T> {

    override fun executeClose() = iterateInfo.close()

    @Volatile
    private var iteratedOnce = false

    private inline fun <R> synchronized(block: () -> R) =
            synchronized(lock = this, block = block)

    override fun iterator(): Iterator<T> {

        fun throwIfAlreadyClosed() =
                iteratedOnce.ifTrue { throw IllegalStateException("Already iterated") }

        throwIfAlreadyClosed()
        synchronized(this) {
            throwIfAlreadyClosed()
            iteratedOnce = true
        }

        return object : Iterator<T> {

            private var didNext = false
            private var hasNext = false

            private fun switchToNextIfNeedUnsafe(): Boolean {
                didNext.ifFalse {
                    hasNext = iterateInfo.switchLine()
                            .apply { ifFalse { iterateInfo.close() } }
                    didNext = true
                }
                return hasNext;
            }

            @Synchronized
            override fun hasNext() =
                    synchronized { switchToNextIfNeedUnsafe() }

            @Synchronized
            override fun next(): T = synchronized {
                switchToNextIfNeedUnsafe().ifFalse {
                    throw java.lang.IllegalStateException("There are no more items")
                }
                didNext = false
                return iterateInfo.extract()
            }

        }
    }

}