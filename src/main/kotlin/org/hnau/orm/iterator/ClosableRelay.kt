package org.hnau.orm.iterator

import org.hnau.base.extensions.boolean.checkBoolean
import org.hnau.base.extensions.pass
import java.io.Closeable


abstract class ClosableRelay : AutoCloseable {

    @Volatile
    private var closed = false

    protected fun <R> checkIsClosed(
            ifClosed: () -> R,
            ifNotClosed: () -> R
    ) = closed.checkBoolean(
            ifTrue = ifClosed,
            ifFalse = {
                synchronized(this) {
                    closed.checkBoolean(
                            ifTrue = ifClosed,
                            ifFalse = ifNotClosed
                    )
                }
            }
    )

    override fun close() = checkIsClosed(
            ifClosed = ::pass,
            ifNotClosed = ::forceClose
    )

    protected fun forceClose() {
        closed = true
        executeClose()
    }

    protected abstract fun executeClose()

}